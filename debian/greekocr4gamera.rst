===============
greekocr4gamera
===============

---------------------------------------------------------
OCR system using the Gamera framework for polytonal Greek
---------------------------------------------------------

:Manual section: 1

Usage
-----

**greekocr4gamera** -x `<traindata>` [`options`] `<imagefile>`

Options
-------
-w, --wholistic
   Use wholistic segmentation mode. This is the default.
-s, --separatistic
   Use separatistic segmentation mode.
-u <file>, --unicode <file>
   Use this filename for Unicode output.
-t <file.tex>, --teubner <file.tex>
   Use this filename for teubner TeX output.
--deskew
   Do a skew correction (recommended).
--filter
   Filter out very large (images) and very small components (noise).
-d, --debug
   Save debug-images: debug_lines.png debug_words.png debug_chars.png
-h, --help
   Display help and exit.
